package com.company.homeworks.bean;

import java.util.Date;

public class Employee {

    private int id;
    private String name;
    private String gender;
    private double salary;
    private double bonus;
    private Date join_date;

    public Employee() {
    }

    public Employee(int id, String name, String gender, double salary, double bonus, Date join_date) {
        this.id = id;
        this.name = name;
        this.gender = gender;
        this.salary = salary;
        this.bonus = bonus;
        this.join_date = join_date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public double getBonus() {
        return bonus;
    }

    public void setBonus(double bonus) {
        this.bonus = bonus;
    }

    public Date getJoin_date() {
        return join_date;
    }

    public void setJoin_date(Date join_date) {
        this.join_date = join_date;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", gender='" + gender + '\'' +
                ", salary=" + salary +
                ", bonus=" + bonus +
                ", join_date=" + join_date +
                '}';
    }
}
