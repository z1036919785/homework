package com.company.homeworks;

import com.company.homeworks.utils.DBUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PrepareStatementTest {

    public static void main(String[] args) throws SQLException {

        // 1. 获取连接
        Connection connection = DBUtils.getConnection();

        String sql = "select * from employee where name = ? and gender = ?";

        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, "jack");
        preparedStatement.setString(2, "m");

        ResultSet resultSet = preparedStatement.executeQuery();

        // 处理结果集
        while (resultSet.next()) {

            int id = resultSet.getInt("id");
            String name = resultSet.getString("name");
            String gender = resultSet.getString("gender");

            System.out.println("id: " + id + "  name: " + name + "  gender: " + gender);

        }

        DBUtils.close(connection, preparedStatement, resultSet);

    }

}
