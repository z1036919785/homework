package com.lagou.time;

import org.apache.flink.api.common.eventtime.*;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.datastream.WindowedStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.windowing.WindowFunction;
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

/**
 * 1、获取数据源
 * 2、转化
 * 3、声明水印（watermark）
 * 4、分组聚合，调用window的操作
 * 5、保存处理结果
 */
public class WaterMarkDemo {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        // 这里flink默认处理的时间是数据进入Flink的时间。
        // 这里需要显示声明为处理时间为EventTime
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);
        // 声明往外发送wartermark的周期时间为1000ms，默认是200ms
        env.getConfig().setAutoWatermarkInterval(1000L);
        // 设定并行度为1
        env.setParallelism(1);

        DataStreamSource<String> data = env.socketTextStream("linux121", 7777);
        // 第一个String是输入数据的类型，第二个参数Tuple2是输出数据的类型
        SingleOutputStreamOperator<Tuple2<String, Long>> maped = data.map(new MapFunction<String, Tuple2<String, Long>>() {
            @Override
            public Tuple2<String, Long> map(String value) throws Exception {
                String[] split = value.split(",");
                return new Tuple2<String, Long>(split[0], Long.valueOf(split[1]));
            }
        });

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

        SingleOutputStreamOperator<Tuple2<String, Long>> watermarks = maped.assignTimestampsAndWatermarks(new WatermarkStrategy<Tuple2<String, Long>>() {
            @Override
            public WatermarkGenerator<Tuple2<String, Long>> createWatermarkGenerator(WatermarkGeneratorSupplier.Context context) {
                return new WatermarkGenerator<Tuple2<String, Long>>() {
                    // 每一个事件或数据发送过来的时候，就会调用一下onEvent方法
                    // 初始化maxTimeStamp
                    private long maxTimeStamp = Long.MIN_VALUE;

                    @Override
                    public void onEvent(Tuple2<String, Long> event, long eventTimestamp, WatermarkOutput output) {
                        // 取得数据源中的最大时间戳
                        maxTimeStamp = Math.max(maxTimeStamp, event.f1);
                        // 打印输出maxTimeStamp试试
                        System.out.println("maxTimeStamp:" + maxTimeStamp + "...format:" + sdf.format(maxTimeStamp));
                    }

                    // 生成watermark有两种方式，一种是周期性的生成，一种是来一个数据生成一个。
                    // 在大数据的体量下，数据来的很快，应该使用周期性的生成，而不是来一个生成一个，那样太耗费计算性能在生成watermark上了。
                    @Override
                    public void onPeriodicEmit(WatermarkOutput output) {
                        System.out.println("...onPeriodicEmit...");
                        // 这里手动指定最大延迟时间为3秒
                        long maxOutOfOrderness = 3000L;
                        // 根据公式生成wartermark。这里采用emit发射这个词，是因为flink发送wartermark是广播形式的，目的是为了迎合高并发，充分利用各个槽。
                        output.emitWatermark(new Watermark(maxTimeStamp - maxOutOfOrderness));
                    }
                };
            }
            // element.f1是表示根据哪个字段来生成水印，这里是指明根据Tuple2的第二个元素来生成水印
        }.withTimestampAssigner(((element, recordTimestamp) -> element.f1)));

        // 4、分组聚合，调用window的操作
        KeyedStream<Tuple2<String, Long>, String> keyed = watermarks.keyBy(value -> value.f0);
        WindowedStream<Tuple2<String, Long>, String, TimeWindow> windowed = keyed.window(TumblingEventTimeWindows.of(Time.seconds(4)));
        SingleOutputStreamOperator<String> resulted = windowed.apply(new WindowFunction<Tuple2<String, Long>, String, String, TimeWindow>() {
            @Override
            public void apply(String s, TimeWindow window, Iterable<Tuple2<String, Long>> input, Collector<String> out) throws Exception {

                String key = s;
                Iterator<Tuple2<String, Long>> iterator = input.iterator();
                ArrayList<Long> list = new ArrayList<>();
                while (iterator.hasNext()) {
                    Tuple2<String, Long> next = iterator.next();
                    // 将Tuple中的数据封装进list中
                    list.add(next.f1);
                }
                // 优化，对窗口中的数据进行排序操作
                Collections.sort(list);
                String result = "key:" + key + "..." + "list.size:" + list.size() + "...list.first:" + sdf.format(list.get(0)) + "...list.last:" + sdf.format(list.get(list.size() - 1)) + "...window.start:" + sdf.format(window.getStart()) + "...window.end:" + sdf.format(window.getEnd());
                out.collect(result);
            }
        });

        resulted.print();
        env.execute();
    }
}
