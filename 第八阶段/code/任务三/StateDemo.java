package com.lagou.state;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.RichFlatMapFunction;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.util.Collector;

/**
 * 案例：利用state求平均值
 * 原始数据：（1,3）（1,5）（1,7）（1,4）（1,2）
 * 思路：
 * 1、读数据源
 * 2、将数据源根据key分组
 * 3、按照key分组策略，对流式数据调用状态化处理
 */
public class StateDemo {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        // 每2秒钟触发一次checkpoint
        env.enableCheckpointing(2000);
        //        DataStreamSource<Tuple2<Long, Long>> data = env.fromElements(new Tuple2(1L, 3L), new Tuple2(1L, 5L), new Tuple2(1L, 7L), new Tuple2(1L, 4L), new Tuple2(1L, 2L));
        // 为了方便观察，数据不再写死，而是从socket中获取
        DataStreamSource<String> data = env.socketTextStream("linux121", 7777);
        // 数据转换成二元组的形式
        SingleOutputStreamOperator<Tuple2<Long, Long>> maped = data.map(new MapFunction<String, Tuple2<Long, Long>>() {
            @Override
            public Tuple2<Long, Long> map(String value) throws Exception {
                String[] split = value.split(",");
                return new Tuple2<Long, Long>(Long.valueOf(split[0]), Long.valueOf(split[1]));
            }
        });

//        KeyedStream<Tuple2<Long, Long>, Long> keyed = data.keyBy(value -> value.f0);
        KeyedStream<Tuple2<Long, Long>, Long> keyed = maped.keyBy(value -> value.f0);

        // 按照key分组策略，对流式数据调用状态化处理
        // 这里，因为不仅要保存Tuple数据，还要保存状态数据，所以传统的一个FlatMapFunction不够用，要用RichFlatMapFunction
        // 使用RichFlatMapFunction最主要原因是里面有open方法，可以去实例化state
        SingleOutputStreamOperator<Tuple2<Long, Long>> flatMaped = keyed.flatMap(new RichFlatMapFunction<Tuple2<Long, Long>, Tuple2<Long, Long>>() {
            ValueState<Tuple2<Long, Long>> sumState;

            // 在open初始化函数里构造state
            @Override
            public void open(Configuration parameters) throws Exception {
                // 首先构建一个descriptor
                ValueStateDescriptor<Tuple2<Long, Long>> descriptor = new ValueStateDescriptor<>(
                        "average",  // 状态名字
                        TypeInformation.of(new TypeHint<Tuple2<Long, Long>>() {
                        }), // 状态类型
                        Tuple2.of(0L, 0L)   // 状态初始值
                );
                // 根据descriptor构建state
                sumState = getRuntimeContext().getState(descriptor);
                super.open(parameters);
            }

            // 每来一个数据，flatMap就会调用一次，update方法要放在这里，在flatMap中更新state
            @Override
            public void flatMap(Tuple2<Long, Long> value, Collector<Tuple2<Long, Long>> out) throws Exception {
                // 获取state状态值
                Tuple2<Long, Long> currentSum = sumState.value();
                // 状态化处理
                currentSum.f0 += 1;
                currentSum.f1 += value.f1;
                // 更新state值
                sumState.update(currentSum);

                // 当数据都到齐了之后，计算平均值
                if (currentSum.f0 == 2) {
                    long average = currentSum.f1 / currentSum.f0;
                    // 将数据发送出去，按照指定的Tuple2格式
                    out.collect(new Tuple2<>(value.f0, average));
                    // 清空state
                    sumState.clear();
                }
            }
        });

//        flatMaped.print();
        // 采用自定义的sink，结合opertorState
        flatMaped.addSink(new OperatorStateDemo(5));
        env.execute();

    }
}
