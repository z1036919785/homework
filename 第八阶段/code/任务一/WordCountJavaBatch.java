package com.lagou;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.operators.AggregateOperator;
import org.apache.flink.api.java.operators.DataSource;
import org.apache.flink.api.java.operators.FlatMapOperator;
import org.apache.flink.api.java.operators.UnsortedGrouping;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.util.Collector;

/**
 * 1、读取数据源
 * 2、处理数据源
 * a、将读到的数据源文件中的每一行根据空格切分
 * b、将切分好的每个单词拼接1
 * c、根据单词聚合（将相同的单词放在一起）
 * d、累加相同的单词（单词后面的1进行累加）
 * 3、保存处理结果
 */
public class WordCountJavaBatch {
    public static void main(String[] args) throws Exception {
        String inputPath = "E:\\wc.txt";
        String outputPath = "E:\\test\\output";

        // 获取flink的运行环境
        ExecutionEnvironment executionEnvironment = ExecutionEnvironment.getExecutionEnvironment();
        // 读取文本文件
        DataSource<String> text = executionEnvironment.readTextFile(inputPath);
        // 转换成一个个(word, 1)的二元组
        FlatMapOperator<String, Tuple2<String, Integer>> wordAndOnes = text.flatMap(new SplitClz());
        // 这里的0表示根据二元组里的第0个元素聚合，这里是根据单词key聚合
        UnsortedGrouping<Tuple2<String, Integer>> groupedWordAndOne = wordAndOnes.groupBy(0);
        // 聚合后，相同key的在一起，开始统计单词数。这里1表示根据二元组里的第二个值进行统计
        AggregateOperator<Tuple2<String, Integer>> out = groupedWordAndOne.sum(1);

        // 输出，这里参数有三种方式选择。这里使用最复杂的一种，接收三个参数的
        // 第一个参数是输出路径，第二个是每一行的分隔符，第三个是每个单词间的分隔符
        // setParallelism表示设置并行度
        out.writeAsCsv(outputPath, "\n", " ").setParallelism(1);

        // 最后人为的开始调度，抛出异常
        executionEnvironment.execute();
    }

    /**
     * 第一个泛型String：表示读到的数据源的每一行的数据类型，
     * 第二个泛型：Tuple2<String, Integer>: 表示转换后输出的结果的数据类型，这里使用一个二元组，输出<word, 1>的形式
     */
    static class SplitClz implements FlatMapFunction<String, Tuple2<String, Integer>>{

        /**
         *
         * @param s 就是读取到的每一行元素
         * @param collector 就是转换后，向下游输出的结果对象
         * @throws Exception
         */
        @Override
        public void flatMap(String s, Collector<Tuple2<String, Integer>> collector) throws Exception {
            // 将读取到的一行文本按照空格切分
            String[] s1 = s.split(" ");
            // 将切分后的每个单词，组成(word, 1)的形式
            for (String word : s1) {
                // 使用collector对象直接发送给下游算子
                collector.collect(new Tuple2<String, Integer>(word, 1));
            }
        }
    }
}
