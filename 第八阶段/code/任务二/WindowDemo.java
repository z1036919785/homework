package com.lagou.window;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.datastream.WindowedStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.source.SourceFunction;
import org.apache.flink.streaming.api.functions.windowing.WindowFunction;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;

import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.Random;


public class WindowDemo {
    public static void main(String[] args) throws Exception {
        // 获取数据源
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        DataStreamSource<String> data = env.addSource(new SourceFunction<String>() {
            int count = 0;

            @Override
            public void run(SourceContext<String> ctx) throws Exception {
                while (true) {
                    ctx.collect(count + "号数据源");
                    count++;
                    Thread.sleep(1000);
                }
            }

            @Override
            public void cancel() {

            }
        });

        // 2、获取窗口
        SingleOutputStreamOperator<Tuple3<String, String, String>> maped = data.map(new MapFunction<String, Tuple3<String, String, String>>() {
            @Override
            public Tuple3<String, String, String> map(String value) throws Exception {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
                long l = System.currentTimeMillis();
                String dataTime = sdf.format(l);
                Random random = new Random();
                int randomNum = random.nextInt(5);
                return new Tuple3<>(value, dataTime, String.valueOf(randomNum));
            }
        });
        // 为了增加并行度，进行keyBy聚合操作，相同key数据会进入同一个分区，给同一个subtask任务
        KeyedStream<Tuple3<String, String, String>, String> keyByed = maped.keyBy(value -> value.f0);
        // 每5s割出一个窗口
        WindowedStream<Tuple3<String, String, String>, String, TimeWindow> timeWindow = keyByed.timeWindow(Time.seconds(5));

        // 3、操作窗口数据
        // 第一个参数Tuple3是窗口输入进来的数据类型，第二个参数Object是输出的数据类型，第三个参数String是数据源中key的数据类型，第四个参数指明当前处理的窗口是什么类型的窗口
        SingleOutputStreamOperator<String> applyed = timeWindow.apply(new WindowFunction<Tuple3<String, String, String>, String, String, TimeWindow>() {
            // s就是上面一行一行的数据源，window代表当前窗口，
            // 一个窗口中数据源可能是相同的，根据keyBy分组的，如果有两个数据源相同，就会放入这个input迭代器里，
            // out将处理结果往外发送
            @Override
            public void apply(String s, TimeWindow window, Iterable<Tuple3<String, String, String>> input, Collector<String> out) throws Exception {
                Iterator<Tuple3<String, String, String>> iterator = input.iterator();
                // new 一个StringBuilder去做字符串拼接
                StringBuilder sb = new StringBuilder();
                while (iterator.hasNext()) {
                    // 这个next就是一个一个Tuple3数据
                    Tuple3<String, String, String> next = iterator.next();
                    sb.append(next.f0 + "..." + next.f1 + "..." + next.f2);
                }
                // 拼接输出的信息，
                String s1 = s + "..." + window.getStart() + "..." + sb;
                out.collect(s1);
            }
        });

        applyed.print();
        env.execute();

    }
}
