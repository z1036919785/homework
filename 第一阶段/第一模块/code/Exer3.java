import java.util.Arrays;

public class Exer3 {

    public static void main(String[] args) {

        /*
        作业3
        需求：定义一个含有五个元素的数组,并为每个元素赋值,求数组中所有元素的最小值
        要求:
        1.定义5个元素数组
        2.可以使用初始化数组的两种方式之一为数组元素赋值
        3.遍历数组求数组中的最小值
         */
        int[] array = new int[]{10, 20, 30, 40, 50};
        int min = 10000;

        for (int i = 0; i < array.length; i++) {
             min = min < array[i] ? min : array[i];
        }
        System.out.println(Arrays.toString(array) + " 最小值为: " + min);
    }
}
