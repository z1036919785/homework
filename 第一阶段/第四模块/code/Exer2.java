package com.company.homework.coreclass2;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

/**
 * 题目：编写程序，循环接收用户从键盘输入多个字符串，
 * 直到输入“end”时循环结束，并将所有已输入的字符串按字典顺序倒序录入到项目下abc.txt中
 */
public class Exer2 {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        StringBuilder sb = new StringBuilder();
        System.out.println("输入字符串: (end结束输入)");
        while (true) {
            String str = sc.next();
            if ("end".equals(str)) {
                break;
            }
            StringBuilder appendStr = sb.append(str);
        }
        // 转为char[]数组用于排序
        char[] charArray = sb.toString().toCharArray();
        Arrays.sort(charArray);
        String s = Arrays.toString(charArray);
        StringBuilder stringBuilder = new StringBuilder(s);
        // 替换空格, 逗号, []
        String resultStr = stringBuilder.reverse().toString().replaceAll("( ,|\\[|\\])", "");
        System.out.println("排序后: " + resultStr);

        File file = new File("abc.txt");
        FileWriter fw = null;
        BufferedWriter bw = null;

        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        // 讲结果写入abc.txt
        try {
            fw = new FileWriter(file);
            bw = new BufferedWriter(fw);
            bw.write(resultStr);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (null != bw) {
                try {
                    bw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(null != fw) {
                try {
                    fw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }

    }
}
