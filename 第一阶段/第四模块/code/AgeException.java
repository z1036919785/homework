package com.company.homework.coreclass2;

public class AgeException extends Exception {

    static final long serialVersionUID = -3387516993124229348L;

    public AgeException(String message) {
        super(message);
    }
}
