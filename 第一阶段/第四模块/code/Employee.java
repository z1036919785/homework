package com.company.homework.coreclass2;

/**
 * 编写一个员工类  (1)属性: 姓名,年龄      
 * 如果年龄小于18龄大于150抛出年龄异常      
 * 如果姓名为null或者""抛出空异常
 */
public class Employee {
    private String name;
    private int age;

    public Employee(String name, int age) throws AgeException {
        setName(name);
        setAge(age);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if ("".equals(name) || null == name) {
            throw new RuntimeException("姓名不能为空哟~");
        } else {
            this.name = name;
        }

    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) throws AgeException {
        if (age > 18 && age < 150) {
            this.age = age;
        } else {
//            throw new RuntimeException("年龄不合理哦~");
            throw new AgeException("年龄不合理哦~");
        }
    }
}
