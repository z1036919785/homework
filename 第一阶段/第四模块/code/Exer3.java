package com.company.homework.coreclass2;

/**
 * 火车买票案例 (两种实现方式) :
 * 共100张票,三个线程一起卖票,不允许出现出现负票和多卖的现象  
 * (描述设置锁的时候要注意的事项:(Thread 和Runnable)
 */
public class Exer3 {

    /*
    设置锁要使锁是唯一的, 注意共享变量
    继承Thread类的方式因为创建多个线程会创建多个Windows对象, 所以使用this会有三把不同的锁, 需要使用 .class作为锁
    这里的count要用static修饰, 否则会出现重复卖票的情况

    实现Runnable接口的方式只需创建一个Windows对象, 所以可以使用this, 这时锁是唯一的.
     */


    public static void main(String[] args) {
        // 第一种继承Thread
        WindowsExtendsThread w1 = new WindowsExtendsThread();
        WindowsExtendsThread w2 = new WindowsExtendsThread();
        WindowsExtendsThread w3 = new WindowsExtendsThread();

        w1.start();
        w2.start();
        w3.start();

        // 第二种实现Runnable接口
//        WindowsImplRunnable w = new WindowsImplRunnable();
//        Thread thread1 = new Thread(w);
//        Thread thread2 = new Thread(w);
//        Thread thread3 = new Thread(w);
//
//        thread1.start();
//        thread2.start();
//        thread3.start();


    }

}
