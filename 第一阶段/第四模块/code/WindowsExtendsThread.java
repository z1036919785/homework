package com.company.homework.coreclass2;

public class WindowsExtendsThread extends Thread {

    private static int count = 100;

    @Override
    public void run() {
        while (count > 0) {
            synchronized (WindowsExtendsThread.class) {
                if (count == 0) {
                    break;
                }
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("窗口" + Thread.currentThread().getName() + "卖了第" + count + "张票");
                count = count - 1;
            }
        }
    }
}
