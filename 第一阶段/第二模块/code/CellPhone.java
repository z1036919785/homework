package com.company.homework.toobject;

public abstract class CellPhone {
    public abstract void cell();
    public abstract void message();
}
