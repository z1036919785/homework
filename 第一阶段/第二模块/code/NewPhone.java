package com.company.homework.toobject;

public class NewPhone extends CellPhone implements IPlay{
    @Override
    public void cell() {
        System.out.println("打电话");
    }

    @Override
    public void message() {
        System.out.println("发短信");
    }

    @Override
    public void playGame() {
        System.out.println("玩游戏");
    }
}
