package com.company.homework.toobject;

public class Test {

    public static void main(String[] args) {
        Manager manager = new Manager();
        manager.setName("一龙");
        manager.setId(123);
        manager.setSalary(15000);
        manager.setBonus(6000);
        manager.work();

        Coder coder = new Coder("小王", 135, 10000);
        coder.work();

        System.out.println("--------------------------------------------------");

        NewPhone newPhone = new NewPhone();
        newPhone.playGame();

    }
}
