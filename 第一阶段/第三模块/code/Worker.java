package com.company.homework.coreclass;


public class Worker {
    /**
     * 作业2
     * 分析以下需求，并用代码实现：
     * (1)定义一个Worker类型(属性私有、空参有参构造、set、get方法、toString方法、equals方法、hashCode方法)，
     * 包含三个属性：姓名(String name)、年龄(int age)、工资(double salary),
     * 在Worker类中定义work方法打印一句话，如："凤姐 正在工作"，其中"凤姐"代表当前对象的姓名
     * (2)定义List集合，在List集合中增加三个员工，基本信息如下：  
     * "凤姐" 18 20000  "欧阳峰" 60 8000  "刘德华" 40 30000
     * (3)在"欧阳峰"之前插入一个员工，信息为：姓名："张柏芝"，年龄：35，工资3300  
     * (4)删除"刘德华"的信息  
     * (5)利用for 循环遍历，打印List 中所有员工的信息
     * (6)利用迭代遍历，对List 中所有的员工调用work 方法  
     * (7)为Worker类重写equals 方法，当姓名、年龄、工资全部相等时候才返回true
     */
    private String name;
    private int age;
    private double salary;

    public Worker() {
    }

    public Worker(String name, int age, double salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Worker worker = (Worker) o;

        if (age != worker.age) return false;
        if (Double.compare(worker.salary, salary) != 0) return false;
        return name != null ? name.equals(worker.name) : worker.name == null;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = name != null ? name.hashCode() : 0;
        result = 31 * result + age;
        temp = Double.doubleToLongBits(salary);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "Worker{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", salary=" + salary +
                '}';
    }

    public void work() {
        System.out.println(name + " 正在工作");
    }

}
