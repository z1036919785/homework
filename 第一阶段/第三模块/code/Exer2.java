package com.company.homework.coreclass;

import java.util.ArrayList;
import java.util.List;

public class Exer2 {

    public static void main(String[] args) {
        /**
         * 作业2
         * 分析以下需求，并用代码实现：
         * (1)定义一个Worker类型(属性私有、空参有参构造、set、get方法、toString方法、equals方法、hashCode方法)，
         * 包含三个属性：姓名(String name)、年龄(int age)、工资(double salary),
         * 在Worker类中定义work方法打印一句话，如："凤姐 正在工作"，其中"凤姐"代表当前对象的姓名
         * (2)定义List集合，在List集合中增加三个员工，基本信息如下：  
         * "凤姐" 18 20000  "欧阳峰" 60 8000  "刘德华" 40 30000
         * (3)在"欧阳峰"之前插入一个员工，信息为：姓名："张柏芝"，年龄：35，工资3300  
         * (4)删除"刘德华"的信息  
         * (5)利用for 循环遍历，打印List 中所有员工的信息
         * (6)利用迭代遍历，对List 中所有的员工调用work 方法  
         * (7)为Worker类重写equals 方法，当姓名、年龄、工资全部相等时候才返回true
         */

        // 定义List集合, 并增加员工
        List<Worker> workers = new ArrayList<>();
        Worker worker1 = new Worker("凤姐", 18, 20000);
        Worker worker2 = new Worker("欧阳锋", 60, 8000);
        Worker worker3 = new Worker("刘德华", 40, 30000);
        workers.add(worker1);
        workers.add(worker2);
        workers.add(worker3);
//        System.out.println("增加三个员工: " + workers.toString());

        // 插入员工
        workers.add(workers.indexOf(worker2), new Worker("张柏芝", 35, 3300));
//        System.out.println("在欧阳锋前插入张柏芝: " + workers.toString());

        // 删除刘德华
        workers.remove(worker3);
//        System.out.println("删除刘德华: " + workers.toString());

        // 遍历打印员工信息
        for (Worker worker : workers) {
            System.out.println(worker.toString());
        }

        // 迭代调用work()方法
        for (Worker worker : workers) {
            worker.work();
        }


    }
}
