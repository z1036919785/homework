package com.company.homework.coreclass;

public class Exer1 {
    public static void main(String[] args) {

        /**
         * 作业1
         * 分析以下需求，并用代码实现：  
         * (1)模拟Arrays.toString(int[] arr);方法，自己封装一个public static String toString(int[] arr);  
         * (2)如果int类型数组arr为null，toString方法返回字符串"null"  
         * (3)如果int类型数组arr长度为0，toString方法返回字符串"[]"
         * (4)如果int类型数组arr的内容为{1,2,3,4,5},toString方法返回字符串"[1, 2, 3, 4, 5]"
         */

        System.out.println(Exer1.toString(null));
        System.out.println(Exer1.toString(new int[0]));
        System.out.println(Exer1.toString(new int[]{1, 2, 3, 4, 5, 6}));

    }

    public static String toString(int[] arr) {

        // 1. arr 为null:
        if (null == arr) {
            return "null";
        // 2. arr.lenth == 0:
        } else if (0 == arr.length) {
            return "[]";
        } else {
        // 3. arr.lenth > 0:
            StringBuilder sb = new StringBuilder();
            sb.append("[");
            for (int i = 0; i < arr.length; i++) {
                if (i != (arr.length - 1)) {
                    sb.append(arr[i]).append(",").append(" ");
                } else {
                    sb.append(arr[i]);
                }
            }
            sb.append("]");
            return sb.toString();
        }
    }

}
