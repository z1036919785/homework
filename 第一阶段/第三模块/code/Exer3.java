package com.company.homework.coreclass;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class Exer3 {
    public static void main(String[] args) {
        /**
         * 作业3
         * 分析以下需求，并用代码实现：  
         * (1)定义一个学生类Student，包含属性：姓名(String name)、年龄(int age)  
         * (2)定义Map集合，用Student对象作为key，用字符串(此表示表示学生的住址)作为value  
         * (3)利用四种方式遍历Map集合中的内容，格式：key::value
         */

        HashMap<Student, String> studentMap = new HashMap<>();
        studentMap.put(new Student("小明", 18), "北京");
        studentMap.put(new Student("小王", 18), "北京");
        studentMap.put(new Student("小李", 18), "北京");
        studentMap.put(new Student("小赵", 18), "北京");

        // 第一种
        Set<Student> students = studentMap.keySet();
        for (Student student : students) {
            System.out.println(student + "::" + studentMap.get(student));
        }

        System.out.println("----------------------------------");

        // 第二种
        Set<Map.Entry<Student, String>> entries = studentMap.entrySet();
        for (Map.Entry<Student, String> entry : entries) {
            System.out.println(entry.getKey() + "::" + entry.getValue());
        }

        System.out.println("---------------------------------");

        // 第三种
        Iterator<Map.Entry<Student, String>> iterator = studentMap.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<Student, String> next = iterator.next();
            System.out.println(next.getKey() + "::" + next.getValue());
        }

        System.out.println("---------------------------------");

        // 第四种
        studentMap.forEach((key, value) -> {
            System.out.println(key + "::" + value);
        });


    }
}
