package com.lg.homework;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class RankMapper extends Mapper<LongWritable, Text, RankNum, NullWritable> {

    private RankNum rankNum = new RankNum();

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

        String num = value.toString();

        rankNum.setNum(Integer.parseInt(num));
        rankNum.setRank(-1);
        context.write(rankNum, NullWritable.get());

    }
}
