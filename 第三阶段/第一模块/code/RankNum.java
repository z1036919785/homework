package com.lg.homework;

import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * 利用shuffle阶段的默认排序对ranknum排序
 */
public class RankNum implements WritableComparable<RankNum> {

    private Integer num;
    private Integer rank;

    @Override
    public int compareTo(RankNum o) {
        return this.num - o.num;
    }

    @Override
    public void write(DataOutput dataOutput) throws IOException {
        dataOutput.writeInt(num);
        dataOutput.writeInt(rank);
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        this.num = dataInput.readInt();
        this.rank = dataInput.readInt();
    }

    public RankNum() {
    }

    public RankNum(Integer num, Integer rank) {
        this.num = num;
        this.rank = rank;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    @Override
    public String toString() {
        return
                rank + "\t" + num;
    }
}
