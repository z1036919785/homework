package com.lg.homework;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

public class RankDriver {

    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {

        Configuration configuration = new Configuration();
        Job job = Job.getInstance(configuration, "rankNum");

        // 设置mapper, reducer
        job.setJarByClass(RankDriver.class);
        job.setMapperClass(RankMapper.class);
        job.setReducerClass(RankReducer.class);

        job.setMapOutputKeyClass(RankNum.class);
        job.setMapOutputValueClass(NullWritable.class);

        job.setOutputKeyClass(RankNum.class);
        job.setOutputValueClass(NullWritable.class);

        job.setNumReduceTasks(1);

        FileInputFormat.setInputPaths(job,new Path("D:\\study\\Hadoop\\rank_num_demo\\input"));
        FileOutputFormat.setOutputPath(job,new Path("D:\\study\\Hadoop\\rank_num_demo\\output"));

        boolean flag = job.waitForCompletion(true);
        System.exit(flag ? 0 : -1);

    }

}
