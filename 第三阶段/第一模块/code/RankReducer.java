package com.lg.homework;

import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class RankReducer extends Reducer<RankNum, NullWritable, RankNum, NullWritable> {

    // 设置初始rank
    private int i = 1;

    @Override
    protected void reduce(RankNum key, Iterable<NullWritable> values, Context context) throws IOException, InterruptedException {
        // 对排好序的rankNum对rank进行赋值
        for (NullWritable value : values) {
            key.setRank(i);
            context.write(key,value);
            i++;
        }

    }
}
