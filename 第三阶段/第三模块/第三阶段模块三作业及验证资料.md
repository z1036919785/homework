### 作业: 

```sql
create table user_clicklog(
   user_id string,
    click_time string
)
row format delimited
fields terminated by ",";

load data local inpath '/root/impala_data/clicklog.dat' into table user_clicklog;


with tmp as (
select
    user_id,
    -- 转换为时间戳排序并按此排序
    unix_timestamp(click_time,'yyyy-MM-dd HH:mm:ss') new_dt,
    from_unixtime(unix_timestamp(click_time,'yyyy-MM-dd HH:mm:ss'),'yyyy-MM-dd HH:mm:ss') as dt
from user_clicklog
order by new_dt
),
tmp1 as (
select
    user_id,
    dt,
    lag(dt) over(partition by user_id order by dt asc) as lag_dt
from tmp
),
tmp2 as (
select
    user_id,
    dt,
    lag_dt,
    -- 转为分钟数
    nvl(((unix_timestamp(dt, 'yyyy-MM-dd HH:mm:ss') - unix_timestamp(lag_dt, 'yyyy-MM-dd HH:mm:ss')) / 60), 31) as diff_dt
from tmp1
)
select
    user_id,
    dt,
    -- lag_dt,
    -- diff_dt,
    row_number() over(partition by user_id order by dt asc) as rk
from tmp2
where diff_dt > 30;
```



### 验证资料: 

![image-20211126023620504](C:\Users\1036\AppData\Roaming\Typora\typora-user-images\image-20211126023620504.png)