package com.lg.zk;


import com.mysql.jdbc.Connection;
import org.I0Itec.zkclient.ZkClient;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

@Component
public class Startup {

    @PostConstruct
    public void init() throws IOException, SQLException, ClassNotFoundException {
        ZkClient zkClient = new ZkClient("centos7-0:2181,centos7-1:2181,centos7-2:2181");
        String cfg = zkClient.readData("/webapp/dblinkcfg", true);
        Properties pro = new Properties();
        Utils.loadData(pro, cfg);
        // 创建数据库连接池
        Utils.createDbPool(pro);
        // 监听节点数据的变化
        Listener.monitor();

        // 使用连接池测试
        Connection conn = ConnectionManager.getConnection();

    }

}
