package com.lg.businfo


case class BusInfo(
                    deployNum: String,
                    simNum: String,
                    transpotNum: String,
                    plateNum: String,
                    lglat: String,
                    speed: String,
                    direction: String,
                    mileage: String,
                    timeStr: String,
                    oilRemain: String,
                    weight: String,
                    acc: String,
                    locate: String,
                    oilWay: String,
                    electric: String
                  )
object BusInfo {
  def apply(msg: String): BusInfo = {
    val arr: Array[String] = msg.split(",")
    new BusInfo(
      arr(0),
      arr(1),
      arr(2),
      arr(3),
      arr(4),
      arr(5),
      arr(6),
      arr(7),
      arr(8),
      arr(9),
      arr(10),
      arr(11),
      arr(12),
      arr(13),
      arr(14)
    )
  }
}
