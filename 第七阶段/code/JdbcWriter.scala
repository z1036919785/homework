package com.lg.businfo

//import com.mysql.jdbc.{Connection, PreparedStatement}
import org.apache.spark.sql.ForeachWriter

import java.sql
import java.sql.{Connection, DriverManager, PreparedStatement}



class JdbcWriter extends ForeachWriter[BusInfo] {
  var conn: Connection = _
  var statement: PreparedStatement = _

  // 开启连接
  override def open(partitionId: Long, epochId: Long): Boolean = {
    if (conn == null) {
      conn = JdbcWriter.getConnection
    }
    true
  }

  // 新增数据
  override def process(value: BusInfo): Unit = {
    val arr: Array[String] = value.lglat.split("_")
    val sql = "insert into car_gps(deployNum, plateNum, timeStr, lng, lat) values(?,?,?,?,?)"
    statement = conn.prepareStatement(sql)
    statement.setString(1, value.deployNum)
    statement.setString(2, value.plateNum)
    statement.setString(3, value.timeStr)
    statement.setString(4, arr(0))
    statement.setString(5, arr(1))
    statement.executeUpdate()
  }

  // 关闭连接
  override def close(errorOrNull: Throwable): Unit = {
    if (null != conn) conn.close()
    if (null != statement) statement.close()
  }
}

object JdbcWriter {
  // 初始化连接
  var conn: Connection = _;
  val url = "jdbc:mysql://linux123:3306/lg_logstic?useUnicode=true&characterEncoding=utf8"
  val username = "root"
  val password = "12345678"
  def getConnection: Connection = {
    if (null == conn || conn.isClosed) {
      // 加载驱动
      classOf[com.mysql.jdbc.Driver]
      // 获取连接
      conn = DriverManager.getConnection(url, username, password)
    }
    conn
  }
}
