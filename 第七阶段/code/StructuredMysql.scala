package com.lg.businfo

import org.apache.spark.SparkContext
import org.apache.spark.sql.{DataFrame, Dataset, SparkSession}


object StructuredMysql {
  def main(args: Array[String]): Unit = {
    // 1 获取sparksession
    val spark: SparkSession = SparkSession.builder()
      .master("local[*]")
      .appName(StructuredMysql.getClass.getName)
      .getOrCreate()
    val sc: SparkContext = spark.sparkContext
    sc.setLogLevel("WARN")
    import spark.implicits._

    // 2 读取kafka数据源
    val kafkaDf: DataFrame = spark.readStream
      .format("kafka")
      .option("kafka.bootstrap.servers", "linux123:9092")
      .option("subscribe", "lagou_bus_info")
      .load()

    // 3 处理数据
    val kafkaValDf: DataFrame = kafkaDf.selectExpr("CAST(value AS STRING)")
    // 转为ds
    val kafkaDs: Dataset[String] = kafkaValDf.as[String]
    val busInfoDs: Dataset[BusInfo] = kafkaDs.map(BusInfo(_)).filter(_ != null)

    // 4 输出，写出数据到mysql
    busInfoDs.writeStream
      .foreach(new JdbcWriter)
      .outputMode("append")
      .start()
      .awaitTermination()

  }
}
