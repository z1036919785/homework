package cn.lagou.utils

object IpUtils {

  def ip2Long(ip: String): Long = {
    val ipSplitArr: Array[String] = ip.split("[.]")
    var ipNum = 0L
    for (i <- 0 until ipSplitArr.length){
      ipNum =  ipSplitArr(i).toLong | ipNum << 8L
    }
    ipNum
  }

  def binarySearch(lines: Array[(Long, Long, String)], ip: Long) : Int = {
    var low = 0
    var high = lines.length - 1
    while (low <= high) {
      val middle = (low + high) / 2
      if ((ip >= lines(middle)._1) && (ip <= lines(middle)._2))
        return middle
      if (ip < lines(middle)._1)
        high = middle - 1
      else {
        low = middle + 1
      }
    }
    -1
  }

  def main(args: Array[String]): Unit = {

    val l: Long = ip2Long("1.0.1.0")

    println(l)
  }

}
