package cn.lagou.homework

import java.time.temporal.TemporalAccessor

import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{DataFrame, Row, SparkSession}

import scala.util.matching.Regex

object HomeWork02 {

  def main(args: Array[String]): Unit = {

    val spark: SparkSession = SparkSession.builder().master("local[*]").appName("home work 02").getOrCreate()

    val sc: SparkContext = spark.sparkContext

    sc.setLogLevel("warn")

    // 任务2.1: 计算独立ip数
    // 1. 读取文件
    val df: DataFrame = spark.read.format("text").load("C:\\Users\\1036\\IdeaProjects\\LagouBigData\\data\\cdn.txt")

    val rdd: RDD[Row] = df.rdd

    val ipRDD: RDD[String] = rdd.map {
      row =>
        row.toString().split("\\s+")(0).replace("[", "")
    }

    println("独立IP数: " + ipRDD.distinct.collect.length)
    //    println(ipRDD.distinct.count)


    // 任务2.2: 统计每个视频独立IP数(视频的标志: 在日志文件中的某些可以找到*.mp4, 代表一个视频文件)
    // 过滤出*.mp4的记录
    val mp4RDD: RDD[String] = rdd.filter {
      row =>
        //        row.toString().contains(".mp4")
        val str: String = row.getString(0)
        str.contains(".mp4")
    }.map {
      row =>
        row.getString(0)
    }

    val ipAndMp4RDD: RDD[(String, String)] = mp4RDD.map {
      line =>
        val fields: Array[String] = line.split("\\s+")
        (fields(0), fields(6))
    }


    // 将*.mp4和ip提取出来
    val mp4AndIpRDD: RDD[(String, String)] = ipAndMp4RDD.map {
      case (ip, mp4) =>
        (extract(mp4), ip)
    }

    val mp4AndCountRDD: RDD[(String, Int)] = mp4AndIpRDD.distinct.map { case (k, v) => (k, 1) }.reduceByKey(_ + _).sortBy { case (k, count) => -count }

    mp4AndCountRDD.collect.foreach(println)


    // 任务2.3: 统计一天中每个小时的流量
    //    rdd.foreach(println)
    val dateAndIpAddressRDD: RDD[(String, String)] = rdd.mapPartitions {
      rows =>
        val fieldsList = Array
        rows.map {
          row =>
            val str: String = row.getString(0)
            val fields: Array[String] = str.split("\\s+")
            (fields(3).replace("[", "").substring(0, 14), fields(6))
        }
    }

    val dateCount: RDD[(String, Int)] = dateAndIpAddressRDD.map {
      case (date, ipAddress) =>
        (date, 1)
    }.reduceByKey(_ + _).sortBy(-_._2)

    dateCount.foreach(println)

  }


  def extract(text: String): String = {
    val regex = new Regex("(\\d+\\.mp4)")
    val str: String = regex.findAllIn(text).mkString(",")
    if (str.length > 0) {
      str
    } else {
      "null"
    }

  }

}
