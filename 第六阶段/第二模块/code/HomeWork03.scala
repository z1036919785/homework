package cn.lagou.homework

import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession
import org.apache.thrift.Option.None

object HomeWork03 {

  def main(args: Array[String]): Unit = {

    val spark: SparkSession = SparkSession.builder().appName("home work 03").master("local[*]").getOrCreate()

    import spark.implicits._

    val sc: SparkContext = spark.sparkContext

    sc.setLogLevel("warn")

    // 读取数据
    val impRDD: RDD[String] = sc.textFile("C:\\Users\\1036\\IdeaProjects\\LagouBigData\\data\\imp.log")
    val clickRDD: RDD[String] = sc.textFile("C:\\Users\\1036\\IdeaProjects\\LagouBigData\\data\\click.log")

    // 数据转换
    val clickAndOne: RDD[(String, Int)] = clickRDD.mapPartitions {
      iter =>
        iter.map {
          line =>
            ("click: " + line.split("\\&")(4).trim, 1)
        }
    }

    val impAndOne: RDD[(String, Int)] = impRDD.mapPartitions {
      iter =>
        iter.map {
          line =>
            ("imp: " + line.split("\\&")(4).trim, 1)
        }
    }

    val joinRDD: RDD[(String, (Option[Int], Option[Int]))] = clickAndOne.fullOuterJoin(impAndOne)

    val pairRDD: RDD[(String, (Int, Int))] = joinRDD.map {
      case (k, (v1, v2)) =>
        var n1 = v1.getOrElse(0)
        var n2 = v2.getOrElse(0)

//        n1 match {
//          case int: Int => n1
//          case _ => 0
//        }
//
//        n2 match {
//          case int: Int => n2
//          case _ => 0
//        }

        (k, (n1, n2))

    }


//    val pairRDD: RDD[(String, (Int, Int))] = value.map {
//      pair =>
//        (pair._1, (pair._2._1.get.toInt, pair._2._2.get.toInt))
//    }

    pairRDD.foreach(println)
    println("*"*20)
    val reduceByKeyRDD: RDD[(String, (Int, Int))] = pairRDD.reduceByKey {
      case (v1, v2) =>
        println(v1._1, v2._1)
        (v1._1 + v2._1, v1._2 + v2._2)
    }

    val resultRDD: RDD[(String, (Int, Int))] = reduceByKeyRDD.map {
      case (k, v) =>
        (k.split(" ")(1), v)
    }.reduceByKey {
      case (v1, v2) =>
        println(v1._1, v2._1)
        (v1._1 + v2._1, v1._2 + v2._2)
    }

    println("*"*20)
    resultRDD.foreach(println)



  }


}
