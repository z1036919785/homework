package cn.lagou.homework

import org.apache.spark.SparkContext
import org.apache.spark.sql.{DataFrame, SparkSession}

object HomeWork04 {

  def main(args: Array[String]): Unit = {

    val spark: SparkSession = SparkSession.builder().appName("home work 04").master("local[*]").getOrCreate()

    val sc: SparkContext = spark.sparkContext

    sc.setLogLevel("warn")

    val df: DataFrame = spark.read.format("csv").option("header", "true").option("delimiter", " ").load("C:\\Users\\1036\\IdeaProjects\\LagouBigData\\data\\data.csv")

    df.createOrReplaceTempView("dateT")

    spark.sql(
      """
        |with tmp as (
          |select
          |    startdate
          |from
          |    dateT
          |
          |union
          |
          |select
          |    enddate
          |from
          |    dateT
        |  )
        |  ,
        |  tmp1 as (
        |  select
        |      startdate as date
        |  from
        |      tmp
        |  order by date asc
        |  ),
        |  tmp2 as (
        |  select
        |      date as startdate,
        |      lead(date) over(order by date asc) as enddate
        |  from
        |      tmp1
        |  )
        |  select
        |   *
        |   from tmp2
        |   where enddate is not null
        |
    """.stripMargin).show



  }


}
