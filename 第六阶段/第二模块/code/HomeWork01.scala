package cn.lagou.homework

import cn.lagou.utils.IpUtils
import org.apache.spark.SparkContext
import org.apache.spark.broadcast.Broadcast
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession

object HomeWork01 {

  def main(args: Array[String]): Unit = {

    val spark: SparkSession = SparkSession.builder().appName("home work 01").master("local[*]").getOrCreate()

    val sc: SparkContext = spark.sparkContext

    sc.setLogLevel("warn")

    //1. sparkcore方式
    // 1.1 读取数据.
    val httpRDD: RDD[String] = sc.textFile("C:\\Users\\1036\\IdeaProjects\\LagouBigData\\data\\http.log")
    val ipRDD: RDD[String] = sc.textFile("C:\\Users\\1036\\IdeaProjects\\LagouBigData\\data\\ip.dat")

    // 1.2 将http.log中的ip地址拆分出来,并转转换为ipNum.
    val httpIpNumRdd: RDD[Long] = httpRDD.map {
      case line => {
        IpUtils.ip2Long(line.split("\\|")(1))
      }
    }

    // 1.3 将ip.dat中的ipNum和城市拆分出来, 如果三级城市为空, 则取二级城市.
    val dictRDD: RDD[(Long, Long, String)] = ipRDD.map {
      case line => {
        val fields: Array[String] = line.split("\\|")
        val tuple: (String, String, String) = if (fields(7).trim.length == 0) (fields(2), fields(3), fields(6)) else (fields(2), fields(3), fields(7))
        Tuple3(tuple._1.toLong, tuple._2.toLong, tuple._3)
      }
    }

    // 1.4 将ip.dat广播出去
    val dict: Array[(Long, Long, String)] = dictRDD.collect()
    val dictBroadCast: Broadcast[Array[(Long, Long, String)]] = sc.broadcast(dict)

    // 1.5 将http.log中的ipNum匹配dictBroadCast确定城市
    val cityAndOneRDD: RDD[(String, Int)] = httpIpNumRdd.map {
      var city = "未知"
      val dictBroadCastValue: Array[(Long, Long, String)] = dictBroadCast.value
      ipNum => {
        val index: Int = IpUtils.binarySearch(dictBroadCastValue, ipNum)
        if (index != -1) {
          city = dictBroadCastValue(index)._3
        }
        (city, 1)
      }
    }

    val cityCountRDD: RDD[(String, Int)] = cityAndOneRDD.reduceByKey(_ + _).sortBy(_._2, ascending = false)

    cityCountRDD.collect.foreach(println)

  }


}
