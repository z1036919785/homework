package lagou.api;

import org.apache.kudu.client.*;


public class UpdateDemo {
    public static void main(String[] args) {
        KuduClient.KuduClientBuilder builder = new KuduClient.KuduClientBuilder("linux122");
        builder.defaultSocketReadTimeoutMs(5000);
        KuduClient client = builder.build();

        try {
            KuduTable stuTable = client.openTable("student2");
            KuduSession kuduSession = client.newSession();
            kuduSession.setFlushMode(SessionConfiguration.FlushMode.MANUAL_FLUSH);
            Update update = stuTable.newUpdate();
            PartialRow row = update.getRow();
            row.addInt("id", 1);
            row.addString("name", "xiaoming");
            kuduSession.apply(update);

            kuduSession.close();

        } catch (KuduException e) {
            e.printStackTrace();
        } finally {
            try {
                client.close();
            } catch (KuduException e) {
                e.printStackTrace();
            }
        }
    }
}
