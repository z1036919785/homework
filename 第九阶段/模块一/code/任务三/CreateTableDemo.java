package lagou.api;

import org.apache.kudu.ColumnSchema;
import org.apache.kudu.Schema;
import org.apache.kudu.Type;
import org.apache.kudu.client.CreateTableOptions;
import org.apache.kudu.client.KuduClient;
import org.apache.kudu.client.KuduException;

import java.util.ArrayList;

public class CreateTableDemo {
    public static void main(String[] args) {
        // 生成client
        String masterAddresses = "linux122";
        KuduClient.KuduClientBuilder kuduClientBuilder = new KuduClient.KuduClientBuilder(masterAddresses);
        KuduClient client = kuduClientBuilder.build();

        String tableName = "student2";

        // 指定每一列的信息，生成schema，这里创建了两列，id和name
        ArrayList<ColumnSchema> columnSchemas = new ArrayList<>();
        // 使用.key(true)方法显式设定为主键
        ColumnSchema id = new ColumnSchema.ColumnSchemaBuilder("id", Type.INT32).key(true).build();
        ColumnSchema name = new ColumnSchema.ColumnSchemaBuilder("name", Type.STRING).key(false).build();
//        ColumnSchema age = new ColumnSchema.ColumnSchemaBuilder("age", Type.INT32).key(false).build();
        columnSchemas.add(id);
        columnSchemas.add(name);
//        columnSchemas.add(age);
        Schema schema = new Schema(columnSchemas);

        // 指定options设置
        CreateTableOptions options = new CreateTableOptions();
        // 设定当前的副本数量为1
        options.setNumReplicas(1);
        ArrayList<String> colrule = new ArrayList<>();
        // 指定按照哪一列来进行hash分区
        colrule.add("id");
        // 进行hash分区
        options.addHashPartitions(colrule, 3);

        try {
            // 开始建表
            client.createTable(tableName, schema, options);
        } catch (KuduException e) {
            e.printStackTrace();
        } finally {
            try {
                // 关闭连接
                client.close();
            } catch (KuduException e) {
                e.printStackTrace();
            }
        }
    }
}
