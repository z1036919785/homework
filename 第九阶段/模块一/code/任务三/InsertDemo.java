package lagou.api;

import org.apache.kudu.client.*;


public class InsertDemo {
    public static void main(String[] args) {
        // 1、获取客户端
        KuduClient.KuduClientBuilder builder = new KuduClient.KuduClientBuilder("linux122");
        // 设置客户端读取数据的超时时间为5000毫秒
        builder.defaultSocketReadTimeoutMs(5000);
        KuduClient client = builder.build();

        try {
            // 2、打开一张表
            KuduTable stuTable = client.openTable("student2");
            // 3、创建会话
            KuduSession kuduSession = client.newSession();
            // 4、设置刷新数据模式
            kuduSession.setFlushMode(SessionConfiguration.FlushMode.MANUAL_FLUSH);
            // 5、获取插入实例
            Insert insert = stuTable.newInsert();
            // 6、声明待插入数据
            insert.getRow().addInt("id", 1);
            insert.getRow().addString("name", "lucas");
            // 7、刷入数据
            kuduSession.flush();
            // 8、应用插入实例
            kuduSession.apply(insert);
            // 9、关闭会话
            kuduSession.close();
        } catch (KuduException e) {
            e.printStackTrace();
        } finally {
            try {
                client.close();
            } catch (KuduException e) {
                e.printStackTrace();
            }
        }

    }
}
