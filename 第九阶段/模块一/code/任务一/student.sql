-- 创建表
create table student 
(id UInt8, name String, birth Date ) 
engine = MergeTree 
partition by toYYYYMM(birth) 
order by id;

-- 插入数据
insert into student values ( 1, 'zhangsan', '2021-10-01');
insert into student values ( 2, 'lisi', '2021-11-01');
insert into student values ( 3, 'wangwu', '2021-10-03');

-- 查询数据
select * from student;

-- 修改数据
alter table student update name='bailishouyue' where id =1;

-- 删除数据
alter table student delete where id = 3;