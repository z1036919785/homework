package com.lagou.druidhomework;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer;
import org.apache.flink.util.Collector;


import java.util.Properties;

/**
 * @author doublexi
 * @date 2021/11/21 22:18
 * @description 将数据从kafka中读取出来，经过Flink预处理，输出指定格式的数据，打回到kafka中。
 */
public class DruidKafkaETL {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        String sourceTopic = "lagoudruid3";
        String targetTopic = "lagoudruid4";
        Properties props = new Properties();
        props.setProperty("bootstrap.servers", "linux121:9092");
        props.setProperty("auto.offset.reset", "earliest");

        // 生成一个consumer对象
        FlinkKafkaConsumer<String> consumer = new FlinkKafkaConsumer<>(sourceTopic, new SimpleStringSchema(), props);
        // 根据consumer生成DataStreamSource对象
        DataStreamSource<String> data = env.addSource(consumer);

        // 对json串进行格式转换，压平，变为一个订单一个product的json串
        SingleOutputStreamOperator<String> flatMaped = data.flatMap(new FlatMapFunction<String, String>() {
            @Override
            public void flatMap(String value, Collector<String> out) throws Exception {
                // 将value转换为json对象，方便操作遍历products
                JSONObject jsonObject = JSON.parseObject(value);
                JSONArray products = jsonObject.getJSONArray("products");
                // 变量products列表，组成一个订单一个product的json形式串
                for (Object product : products) {
                    // 将原json串克隆一份
                    JSONObject clone = (JSONObject) jsonObject.clone();
                    // 将json串里的products列表替换为单个product
                    clone.remove("products");
                    clone.put("product", product);
                    // 将转换好的json对象转换为json字符串发送出去。
                    out.collect(clone.toJSONString());
                }
            }
        });

        // 打印转换好的数据观察测试
        flatMaped.print();
        // 将转换好的json串，发送到kafka第二个主题lagoudruid4中。
        FlinkKafkaProducer<String> flinkKafkaProducer = new FlinkKafkaProducer<>(targetTopic, new SimpleStringSchema(), props);
        // 下沉到kafka中
        flatMaped.addSink(flinkKafkaProducer);

        env.execute();
    }
}
