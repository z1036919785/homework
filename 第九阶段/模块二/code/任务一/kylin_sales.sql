-- 创建订单数据库、表结构
create database if not exists `lagou_kylin`;

-- 1、销售表：dw_sales2
-- id		唯一标识
-- date1	日期 分区字段
-- channelId	渠道ID
-- productId	产品ID
-- regionId		区域ID
-- amount		数量
-- price		金额
create table lagou_kylin.dw_sales2(
id string,
channelId string,
productId string,
regionId string,
amount int,
price double
) 
partitioned by (date1 string)
ROW FORMAT DELIMITED FIELDS TERMINATED BY ',';

-- 2、渠道表：dim_channel
-- channelId 	渠道ID
-- channelName	渠道名称
create table lagou_kylin.dim_channel(
  channelId string,
  channelName string 
)ROW FORMAT DELIMITED FIELDS TERMINATED BY ',';

-- 3、产品表：dim_product
create table lagou_kylin.dim_product(
  productId string,
  productName string
)ROW FORMAT DELIMITED FIELDS TERMINATED BY ',';

--4、区域表：dim_region
create table lagou_kylin.dim_region(
  regionId string,
  regionName string
)ROW FORMAT DELIMITED FIELDS TERMINATED BY ',';

--5、时间表：
-- 建表
drop table lagou_kylin.dim_date;
create table lagou_kylin.dim_date2(
dateid string,
dayofyear string,
dayofmonth string,
day_in_year string,
day_in_month string,
weekday string,
week_in_month string,
week_in_year string,
date_type string,
quarter string
)ROW FORMAT DELIMITED FIELDS TERMINATED BY ',';



-- 导入数据
load data local inpath "/root/kylin/sales_20201101.dat" into table lagou_kylin.dw_sales2 partition(date1="2020-11-01");
load data local inpath "/root/kylin/sales_20201102.dat" into table lagou_kylin.dw_sales2 partition(date1="2020-11-02");
load data local inpath "/root/kylin/sales_20201103.dat" into table lagou_kylin.dw_sales2 partition(date1="2020-11-03");
load data local inpath "/root/kylin/sales_20201104.dat" into table lagou_kylin.dw_sales2 partition(date1="2020-11-04");
load data local inpath "/root/kylin/sales_20201105.dat" into table lagou_kylin.dw_sales2 partition(date1="2020-11-05");
load data local inpath "/root/kylin/sales_20201106.dat" into table lagou_kylin.dw_sales2 partition(date1="2020-11-06");
LOAD DATA LOCAL INPATH '/root/kylin/dim_channel_data.txt' OVERWRITE INTO TABLE lagou_kylin.dim_channel;
LOAD DATA LOCAL INPATH '/root/kylin/dim_product_data.txt' OVERWRITE INTO TABLE lagou_kylin.dim_product;
LOAD DATA LOCAL INPATH '/root/kylin/dim_region_data.txt' OVERWRITE INTO TABLE lagou_kylin.dim_region;
LOAD DATA LOCAL INPATH '/root/kylin/dim_date.txt' OVERWRITE INTO TABLE lagou_kylin.dim_date2;